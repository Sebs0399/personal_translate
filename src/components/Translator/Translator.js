import {useState} from 'react';
import axios from 'axios';
import styles from './Translator.module.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import BeatLoader from "react-spinners/BeatLoader";

export default function Translator() {
  const [original, setOriginal] = useState("");
  const [translation, setTranslation] = useState("");
  const [source, setSource] = useState('es');
  const [target, setTarget] = useState('en');
  const [loading, setLoading] = useState(false);

  const notifyError = (message) => toast(message);

  const translate = async () => {
    setLoading(true);
    const encodedParams = new URLSearchParams();
    encodedParams.append("q", original);
    encodedParams.append("target", target);
    encodedParams.append("source", source);

    const options = {
      method: 'POST',
      url: 'https://google-translate1.p.rapidapi.com/language/translate/v2',
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
        'Accept-Encoding': 'application/gzip',
        'X-RapidAPI-Key': 'mavHmo75JFmshVxGkx67HYTYEB1Vp1649fGjsnoC5wTLAL3iod',
        'X-RapidAPI-Host': 'google-translate1.p.rapidapi.com'
      },
      data: encodedParams
    };

    try {
      const response = await axios.request(options)
      const {translatedText} = response.data.data.translations[0];
      setTranslation(translatedText);
      setLoading(false);
    } catch(error) {
     notifyError(error.response.data.message) 
      setLoading(false);
    }
  };

  const switchLanguages = () => {
    const newTarget = source
    const newSource = target
    setTarget(newTarget)
    setSource(newSource)
  }

  const clearStates = () => {
    setOriginal('')
    setTranslation('')
  }

  return (
    <>
      <h1>Traductor</h1>
      <div className={styles.headerContainer}>
        <p>Desde: {source === 'es' ? 'Español' : 'Ingles'}</p>
        <p>Hacia: {target === 'es' ? 'Español' : 'Ingles'} </p>
        <button onClick={() => switchLanguages()}>Invertir idiomas</button>
      </div>
      <div className={styles.container}>
        <textarea 
          placeholder={source === 'es' ? 'Texto a traducir' : 'Text to translate'}
          value={original}
          onChange={(e) => setOriginal(e.target.value)}
        >
        </textarea>
        <button onClick={translate} disabled={loading} >{'=>'}</button>
        <textarea 
          placeholder='Traduccion'
          value={translation}
          disabled
        >
        </textarea>
        <ToastContainer />
      </div>
      {loading ? <BeatLoader /> : null}
      <button onClick={() => clearStates()}>Limpiar</button>
    </>
  );
}

