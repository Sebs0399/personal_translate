import './App.css';
import text_inv from './text_inv.png'
import barbie from './barbie2p.png'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={text_inv} className="App-logo" alt="logo" />
        <img src={barbie} className="App-logo" alt="logo" />
        <a
          className="App-link"
        >
          Te quiero mucho
        </a>
      </header>
    </div>
  );
}

export default App;
